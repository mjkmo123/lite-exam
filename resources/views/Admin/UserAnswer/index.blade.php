@extends('adminlte::page')

@section('content_header')
    <h1>your exam answers</h1>
@endsection

@section('content')
    <div class="row mt-1">
        <table class="table">
            <thead>
            <tr>

                    <th scope="col">name</th>
                <th scope="col">tools</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <th scope="row">{{$d['name']}}</th>
                    <th scope="row">
                        <div class="row">
                            <a href="{{route('user-answer.show',$d->id)}}" class="btn btn-success ml-2">view</a>
                        </div>
                    </th>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
@endsection
