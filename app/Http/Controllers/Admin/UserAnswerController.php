<?php

namespace App\Http\Controllers\Admin;

use App\Exam;
use App\Http\Controllers\Controller;
use App\User_answer;
use Illuminate\Http\Request;

class UserAnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {
        $data = Exam::whereUserId(auth()->id())->get();
        return view('Admin.UserAnswer.index',[
           'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     *
     */
    public function create()
    {
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     */
    public function store()
    {
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     *
     */
    public function show($exam)
    {
        $data = Exam::find($exam)->quests()->with('userAnswers')->with('answers')->get();
        return view('Admin.UserAnswer.view',[
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     *
     */
    public function edit(Exam $exam)
    {
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     *
     */
    public function update(Request $request, Exam $exam)
    {
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     *
     */
    public function destroy(Exam $exam)
    {
        return redirect()->back();
    }
}
