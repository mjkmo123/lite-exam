@extends('adminlte::page')

@section('content_header')

@endsection

@section('content')
    <div class="row">
        @foreach($data as $d)
            <div class="card col-12">
                <div class="card-title">
                    @foreach($d['userAnswers'] as $a)
                        @if($d["id"] == $a["quest_id"])
                            <div class="row">
                                {{\App\User::find($a["user_id"])["name"]}}
                            </div>
                        @endif
                    @endforeach</div>
                <div class="card-title">{!! $d["quest"] !!}</div>
                <div class="card-body">
                    @foreach($d["answers"] as $q)
                        @if($d["id"] == $q["quest_id"])
                            <div class="row">
                                {{$q["answer"]}}
                            </div>
                        @endif
                    @endforeach
                    <div class="row">

                        <div class="col-12">user answers :</div>

                        @foreach($d['userAnswers'] as $a)
                            @if($d["id"] == $a["quest_id"])

                                <div class="col-12">{{$a["answer"]}}</div>

                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

